package com.example.offlinefirstapplication

import android.app.Application
import com.example.offlinefirstapplication.koin.KoinModule.dataModule
import com.example.offlinefirstapplication.koin.KoinModule.uiModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidContext(this@App)
            modules(listOf(dataModule, uiModule))
        }
    }
}