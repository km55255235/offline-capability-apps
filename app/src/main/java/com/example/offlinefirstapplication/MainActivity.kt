package com.example.offlinefirstapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.offlinefirstapplication.databinding.ActivityMainBinding
import com.example.offlinefirstapplication.viewmodel.UserViewModel
import com.google.gson.Gson
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val viewModel: UserViewModel by inject()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel.listUsers().observe(this){
            Log.e("USERSX", Gson().toJson(it))
        }
    }
}