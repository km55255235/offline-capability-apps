package com.example.offlinefirstapplication.api

import com.example.offlinefirstapplication.api.response.UserResponse
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {
    @GET("users")
    fun getUsers(): Call<ArrayList<UserResponse>>
}