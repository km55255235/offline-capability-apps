package com.example.offlinefirstapplication.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.offlinefirstapplication.db.entity.UserEntity

@Dao
interface UserDao {
    @Insert
    fun insert(userEntity: UserEntity)

    @Query("SELECT * FROM table_user ORDER BY itemId DESC")
    fun getAllItem(): List<UserEntity>

    @Delete
    fun delete(userEntity: UserEntity)

    @Query("DELETE FROM table_user WHERE itemId = :idParams")
    fun deleteById(idParams: Long)

    @Query("DELETE FROM table_user")
    fun deleteAll()

}