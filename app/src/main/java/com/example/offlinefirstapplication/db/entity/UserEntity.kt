package com.example.offlinefirstapplication.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.offlinefirstapplication.api.response.UserResponse
import com.example.offlinefirstapplication.model.User

@Entity("table_user")
data class UserEntity(
    @PrimaryKey(autoGenerate = true)
    var itemId: Long? = null,

    @ColumnInfo(name = "login")
    var login: String? = null
)

fun UserEntity.toDomain() = User(id = itemId?.toInt(), login = login)


