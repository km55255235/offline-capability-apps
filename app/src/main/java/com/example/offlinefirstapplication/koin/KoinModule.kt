package com.example.offlinefirstapplication.koin

import android.app.Application
import com.example.offlinefirstapplication.api.ApiClient
import com.example.offlinefirstapplication.db.UserDB
import com.example.offlinefirstapplication.repository.UserRepository
import com.example.offlinefirstapplication.viewmodel.UserViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object KoinModule {
    val Application.dataModule
        get() = module {
            //DATABASE
            single { UserDB.getInstance(get()) }
            factory { get<UserDB>().userDao }

            //API
            single { ApiClient.instance }

            //REPOSITORY
            factory { UserRepository(get(), get())}
        }

    val Application.uiModule
        get() = module {
            viewModel { UserViewModel(get())}
        }
}