package com.example.offlinefirstapplication.model

data class User(
    var id: Int?,
    var login: String?
)