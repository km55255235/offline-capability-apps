package com.example.offlinefirstapplication.repository

import androidx.lifecycle.MutableLiveData
import com.example.offlinefirstapplication.api.ApiService
import com.example.offlinefirstapplication.api.response.UserResponse
import com.example.offlinefirstapplication.api.response.toDomain
import com.example.offlinefirstapplication.db.dao.UserDao
import com.example.offlinefirstapplication.db.entity.UserEntity
import com.example.offlinefirstapplication.db.entity.toDomain
import com.example.offlinefirstapplication.model.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserRepository(private val apiService: ApiService, private val userDao: UserDao) {

    var listUsers = MutableLiveData<List<User>>()

    fun getUsers() {
        apiService.getUsers()
            .enqueue(object : Callback<ArrayList<UserResponse>> {
                override fun onResponse(
                    call: Call<ArrayList<UserResponse>>,
                    response: Response<ArrayList<UserResponse>>
                ) {
                    if (response.isSuccessful) {
                        if (!response.body().isNullOrEmpty()) {
                            val userResponse = response.body()
                            userResponse?.let {
                                val users = arrayListOf<User>()
                                userDao.deleteAll()
                                it.forEach {
                                    users.add(it.toDomain())
                                    userDao.insert(UserEntity(login = it.login))
                                }
                                listUsers.postValue(users)
                            }
                        } else {
                            getLocalItem()
                        }
                    } else {
                        getLocalItem()
                    }
                }

                override fun onFailure(call: Call<ArrayList<UserResponse>>, t: Throwable) {
                    getLocalItem()
                }

            })
    }

    fun getLocalItem() {
        val localData = userDao.getAllItem()
        val users = arrayListOf<User>()
        localData.forEach {
            users.add(it.toDomain())
        }
        listUsers.postValue(users)
    }

}