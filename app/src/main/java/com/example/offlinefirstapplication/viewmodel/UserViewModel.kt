package com.example.offlinefirstapplication.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.offlinefirstapplication.repository.UserRepository
import kotlinx.coroutines.launch

class UserViewModel(private val repository: UserRepository) : ViewModel() {

    init {
        viewModelScope.launch {
            repository.getUsers()
        }
    }

    fun listUsers() = repository.listUsers
}